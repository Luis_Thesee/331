DROP TABLE person CASCADE CONSTRAINTS;
DROP TABLE home CASCADE CONSTRAINTS;
DROP TABLE occupation CASCADE CONSTRAINTS;

--Part 1
CREATE TABLE home (
    hid char(4) PRIMARY KEY,
    address varchar(200));
    
CREATE TABLE occupation (
    oid char(4) PRIMARY KEY,
    type varchar(20),
    salary number(10, 2));

CREATE TABLE person (
    pid char(4) PRIMARY KEY,
    firstname varchar(20),
    lastname varchar(20),
    father_id char(4) REFERENCES person(pid),
    mother_id char(4) REFERENCES person(pid),
    hid char(4) REFERENCES home(hid),
    oid char(4) REFERENCES occupation(oid));
    
--Part 2
INSERT INTO home values('H001', '123 Easy St.');
INSERT INTO home values('H002', '56 Fake Ln.');

INSERT INTO occupation values('O000', 'N/A', 0);
INSERT INTO occupation values('O001', 'Student', 0);
INSERT INTO occupation values('O002', 'Doctor', 100000);
INSERT INTO occupation values('O003', 'Professor', 80000);

INSERT INTO person values('0001', 'Zachary', 'Aberny', null, null, 'H002', 'O000');
INSERT INTO person values('0002', 'Yanni', 'Aberny', null, null, 'H002', 'O000');
INSERT INTO person values('0003', 'Alice', 'Aberny', '0001', '0002', 'H001', 'O002');
INSERT INTO person values('0004', 'Bob', 'Bortelson', null, null, 'H001', 'O003');
INSERT INTO person values('0005', 'Carl', 'Aberny-Bortelson', '0003', '0004', 'H001', 'O001');
INSERT INTO person values('0006', 'Denise', 'Aberny-Bortelson', '0003', '0004', 'H001', 'O001');
    

SELECT gp.firstname FROM person gp JOIN person p ON gp.pid = p.mother_id OR gp.pid = p.father_id JOIN person c ON p.pid = c.mother_id OR p.pid = c.father_id WHERE c.firstname = 'Denise';

--Part 3

--1
SELECT address, COUNT(pid) AS "Number of people in that address" FROM home h
JOIN person p ON h.hid = p.hid GROUP BY address;

--2
SELECT DISTINCT gp.firstname,gp.lastname FROM person d
JOIN Person m ON d.father_id = m.pid OR d.mother_id = m.pid
JOIN Person gp ON m.father_id = gp.pid;

--3
SELECT firstname, lastname FROM person p JOIN occupation o ON p.oid = o.oid
WHERE type = 'Student' AND (father_id IS NOT NULL OR mother_id IS NOT NULL);

--4
SELECT MAX(SUM(salary)) AS "House income" 
FROM person p JOIN occupation o ON p.oid = o.oid
GROUP BY hid;

 





    
