--PART 2
CREATE OR REPLACE PACKAGE book_store AS
    FUNCTION get_price_after_tax(book_isbn VARCHAR2) RETURN NUMBER;
END book_store;
/

CREATE OR REPLACE PACKAGE BODY book_store AS
    FUNCTION price_after_discount(book_isbn VARCHAR2) 
    RETURN NUMBER IS 
        discount NUMBER;
    BEGIN
        SELECT (retail-discount) INTO discount FROM books
        WHERE isbn = book_isbn;
        RETURN discount;
    END;
    FUNCTION get_price_after_tax(book_isbn VARCHAR2)
    RETURN NUMBER IS 
        book_taxed NUMBER;
    BEGIN 
        SELECT (retail*1.15) INTO book_taxed FROM books
        WHERE isbn = book_isbn;
        RETURN book_taxed;
    END;
END book_store;

/
    
DECLARE 
    book1_isbn VARCHAR2(200);
    book2_isbn VARCHAR2(200);
    book1_price NUMBER;
    book2_price NUMBER;
BEGIN 
    SELECT isbn INTO book1_isbn FROM books
    WHERE title = 'BUILDING A CAR WITH TOOTHPICKS';
    
    SELECT isbn INTO book2_isbn FROM books
    WHERE title = 'HOLY GRAIL OF ORACLE';
    
    book1_price := book_store.get_price_after_tax(book1_isbn);
    book2_price := book_store.get_price_after_tax(book2_isbn);
    
    dbms_output.put_line(book2_price);
    dbms_output.put_line(book1_price);
END;

/